#ExternalMatch UTILITY

## Description
The ExternalMatch Utility is the tool developed to external match for entities. This utility matches the attrubutes associated to an entity passed in the input file with the existing entities present in the tenant. 

##Change Log

```#!plaintext
Update Date: 10/03/2019
Version: 1.0.7
Description: Added client credential , Reltio Core CST v1.4.9 updated.

Last Update Date: 04/07/2019
Version: 1.0.6
Description: Added proxy channge

Last Update Date: 04/01/2019
Version: 1.0.5
Description: Updated the code with passwaord encrytion and proper validation message

Last Update Date: 02/27/2019
Version: 1.0.4
Description: Removed dependency tika-java7 dependency

Last Update Date: 12/26/2018
Version: 1.0.3
Description: Standarization of Logs to Log4j2,Removed unused dependency

Last Update Date: 06/09/2018
Version: 1.0.2
Description: Initial version
```
##Contributing 
Please visit our [Contributor Covenant Code of Conduct](https://bitbucket.org/reltio-ondemand/common/src/a8e997d2547bf4df9f69bf3e7f2fcefe28d7e551/CodeOfConduct.md?at=master&fileviewer=file-view-default) to learn more about our contribution guidlines

## Licensing
```#!plaintext
Copyright (c) 2017 Reltio

 

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

 

    http://www.apache.org/licenses/LICENSE-2.0

 

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

See the License for the specific language governing permissions and limitations under the License.
```


## Quick Start 
To learn about dependencies, building and executing the tool view our [quick start](https://bitbucket.org/reltio-ondemand/util-externalmatch/src/24d430f548092a0097f6e61e0faa380d1ca7fad1/QuickStart.md?at=master&fileviewer=file-view-default).