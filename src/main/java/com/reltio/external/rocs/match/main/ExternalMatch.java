package com.reltio.external.rocs.match.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.service.impl.SimpleReltioAPIServiceImpl;
import com.reltio.cst.service.impl.TokenGeneratorServiceImpl;
import com.reltio.cst.util.Util;
import com.reltio.file.ReltioFileReader;
import com.reltio.file.ReltioFlatFileReader;


public class ExternalMatch {

	private static final String[] DefaultAttributes = {"InputJson,"+ "MatchRule," + "URI," + "Type," + "Label" };
	
	private static final Logger logger = LogManager.getLogger(ExternalMatch.class.getName());
    private static final Logger logPerformance = LogManager.getLogger("performance-log");

	public static void main(String[] args) throws Exception {
		Properties properties = Util.getProperties(args[0], "PASSWORD", "CLIENT_CREDENTIALS");
	
		String username = properties.getProperty("USERNAME");
		String password = properties.getProperty("PASSWORD");
		String authUrl = properties.getProperty("AUTH_URL");
		String filePath = properties.getProperty("ENTITIES_FILE_PATH");
		String tenant = properties.getProperty("TENANT_ID");
		String apiUrl = properties.getProperty("ENVIRONMENT_URL");
		String outputfile = properties.getProperty("OUTPUT_FILE", "output.csv");
		String attrMappingPath = properties.getProperty("ATTRIBUTE_MAPPING_FILE");

		final Map<String, InputAttribute> attributes = new LinkedHashMap<>();

		final List<String> fileResponseHeader = new ArrayList<String>();
		

		// Validate Attribute
          BufferedReader reader =null;
		if (attrMappingPath != null && !attrMappingPath.isEmpty() && !attrMappingPath.equalsIgnoreCase("null")) {
			reader = new BufferedReader(
					new InputStreamReader(new FileInputStream(attrMappingPath), "UTF-8"));
			createAttributeMapFromProperties(reader, attributes);
			//logger.info("\n" + attributes);
		}
	      // List<String> missingKeys = Util.listMissingProperties(properties,
	               // Arrays.asList("USERNAME", "AUTH_URL", "PASSWORD", "ENTITIES_FILE_PATH", "ENVIRONMENT_URL","TENANT_ID","ATTRIBUTE_MAPPING_FILE"));
	   	  	   
	   	Map<List<String>, List<String>> mutualExclusiveProps = new HashMap<>();

		mutualExclusiveProps.put(Arrays.asList("PASSWORD","USERNAME"), Arrays.asList("CLIENT_CREDENTIALS"));

        List<String> missingKeys = Util.listMissingProperties(properties,Arrays.asList( "AUTH_URL","ENTITIES_FILE_PATH", "ENVIRONMENT_URL","TENANT_ID","ATTRIBUTE_MAPPING_FILE"),mutualExclusiveProps);
         Util.setHttpProxy(properties);

	       if (!missingKeys.isEmpty()) {
	    	   logger.error(
	                    "Following properties are missing from configuration file!! \n" + String.join("\n", missingKeys));
	            System.exit(0);
	        }

		for (String header : DefaultAttributes) {
			fileResponseHeader.add(header);
		}
		// Create Response file header
		for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
			createResponseHeader(attr, fileResponseHeader, null);
		}

		FileWriter filewriter = new FileWriter(outputfile);

        filewriter.write(fileResponseHeader.toString().replaceAll("\\[", "").replaceAll("\\]", "") + "\n");

		// filewriter.write(fileResponseHeader.toString().substring(1, fileResponseHeader.toString().length()-1) + "\n");

        logger.info("Printing header....." + fileResponseHeader.toString());

		TokenGeneratorService service = new TokenGeneratorServiceImpl(username, password, authUrl);
		ReltioAPIService restApi = new SimpleReltioAPIServiceImpl(service);

		StringBuffer requestUrl = new StringBuffer().append("https://").append(apiUrl).append("/reltio/api/").append(tenant)
				.append("/entities/_matches");

		//String requestBody = new String(Files.readAllBytes(Paths.get(filePath)));
		
		ReltioFileReader fileReader = null;
		fileReader = new ReltioFlatFileReader(filePath, "UTF-8");
				
		boolean eof = false;
		String[] requestBodyAll = fileReader.readLine();

		try {
			while(!eof){
				inputFileAttributesExtract(requestBodyAll, fileResponseHeader,
						attributes, requestUrl, restApi, filewriter); 
				requestBodyAll = fileReader.readLine();
				if(requestBodyAll == null)
					eof = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info(e.getMessage());
		} finally {
			filewriter.close();
			fileReader.close();
		}

	}

	private static void inputFileAttributesExtract(String[] requestBodyAll, List<String> fileResponseHeader, Map<String, InputAttribute> attributes, StringBuffer requestUrl, ReltioAPIService restApi, FileWriter filewriter) throws Exception, ReltioAPICallFailureException {
	
		for (int i = 0; i < requestBodyAll.length; i++) {
			String requestBody = requestBodyAll[i];
			Object[] requestBodyObj = new Gson().fromJson(requestBody, Object[].class);

			String headerArray[] = fileResponseHeader.toString().replaceAll("\\[", "").replaceAll("\\]", "").split(",");
			String apiResponse = restApi.post(requestUrl.toString(), requestBody);
			MatchResponse[] matchResponseObj = new Gson().fromJson(apiResponse, MatchResponse[].class);
			
			for (MatchResponse element : matchResponseObj) {

				Iterator<String> itr = element.object.keySet().iterator();
				int inputindex = 0;
				if(itr.hasNext()){
					while (itr.hasNext()) {
						StringBuilder tempLine = new StringBuilder();
						StringBuilder strBuilderValue = new StringBuilder();

						String key = itr.next();
						List<ReltioObject> objList = element.object.get(key);
						logger.info("MatchResponse Key :: " + key + "\nMatchRsponse Value :: " + objList );
							
							// Getting the required response for writing
							Map<String, String> responseMap = getResponse(
									objList, attributes, strBuilderValue );							
							//System.out.println(" Response Map ::  " + responseMap);
							for (int j = 5; j < headerArray.length; j++) {
							String attrtemp = responseMap.get(headerArray[j].trim());
							if (attrtemp == null) attrtemp = "";
							strBuilderValue.append(",").append(attrtemp);
							}
							
							tempLine.append(requestBodyObj[inputindex].toString().replaceAll(",", ";")).append(",").append(key)
							//.append(",").append()
							.append(",").append(strBuilderValue.toString());
							filewriter.write(tempLine.toString() + "\n");							
					}
					inputindex++;
				}
				else
				{
					StringBuilder tempLine = new StringBuilder();
					tempLine.append(requestBodyObj[inputindex++].toString().replaceAll(",", ";").concat("->")+","+"Not Matched");
					filewriter.write(tempLine.toString() + "\n");
				}
			}
			}		
	}

	public static Map<String, String> getResponse(
			List<ReltioObject> objList, Map<String, InputAttribute> attributes, StringBuilder strBuilderValue) {
		Map<String, String> responseMap = new HashMap<String, String>();
		for (ReltioObject reltioObject : objList) {
			strBuilderValue.append(reltioObject.getUri()).append(",").append(reltioObject.getType())
					.append(",").append(reltioObject.getLabel());

			for (Map.Entry<String, InputAttribute> attr : attributes.entrySet()) {
				List<Object> attributeData = reltioObject.attributes.get(attr
						.getKey());
				createExtractOutputData(attr, attributeData, responseMap, null);
			}
		}

		return responseMap;
	}

	/**
	 * @param attribute - attributes
	 * @param data - Lists of data Objects
	 * @param responseMap
	 * @param headerPrefix
	 * 
	 * This method helps in extracting the attribute values which is coming data list
	 *  from match response by comparing the header values
	 * 
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void createExtractOutputData(
			Entry<String, InputAttribute> attribute, List<Object> data,
			Map<String, String> responseMap, String headerPrefix) {

		if (headerPrefix != null) {
			headerPrefix += ".";
		} else {
			headerPrefix = "";
		}
		headerPrefix += attribute.getKey();
		InputAttribute inputAttribute = attribute.getValue();

		// Simple Attribute
		if (inputAttribute.attributesMap == null) {
			int temp = 1;
			if (data != null && !data.isEmpty()) {
				
				for (Object obj : data) {
					if (temp > inputAttribute.count) {
						break;
					}
					com.google.gson.internal.LinkedTreeMap object2 = (LinkedTreeMap) obj;
					String value = (String) object2.get("value");
					@SuppressWarnings("unused")
					Boolean ov = (Boolean) object2.get("ov");

						if (inputAttribute.count > 1) {
							responseMap.put(headerPrefix + "_" + temp++, value);
						} else {
							responseMap.put(headerPrefix, value);
							break;
						}
				}
			}
		} else {
			int temp = 1;
			if (data != null && !data.isEmpty()) {

				for (Object obj : data) {
					if (temp > inputAttribute.count) {
						break;
					}
					com.google.gson.internal.LinkedTreeMap object2 = (LinkedTreeMap) obj;
					Boolean ov = (Boolean) object2.get("ov");
					Map<String, List<Object>> innerAttrs = (Map<String, List<Object>>) object2
							.get("value");

					if (ov) {
						if (inputAttribute.count > 1) {

							for (Map.Entry<String, InputAttribute> inputAttr : inputAttribute.attributesMap
									.entrySet()) {
								List<Object> objects3 = innerAttrs
										.get(inputAttr.getKey());
								createExtractOutputData(inputAttr, objects3,
										responseMap, headerPrefix + "_" + temp);
							}
						} else {

							for (Map.Entry<String, InputAttribute> inputAttr : inputAttribute.attributesMap
									.entrySet()) {
								List<Object> objects3 = innerAttrs
										.get(inputAttr.getKey());
								createExtractOutputData(inputAttr, objects3,
										responseMap, headerPrefix);
							}

							break;
						}
						temp++;
					}
				}
			}
		}
	}

	private static void createResponseHeader(Map.Entry<String, InputAttribute> attr, List<String> fileResponseHeader,
			String headerPrefix) {
		// TODO Auto-generated method stub
		if (headerPrefix != null) {
			headerPrefix += ".";
		} else {
			headerPrefix = "";
		}
		headerPrefix += attr.getKey();
		InputAttribute inputAttribute = attr.getValue();

		if (inputAttribute.count > 1) {
			for (int i = 1; i <= inputAttribute.count; i++) {
				if (inputAttribute.attributesMap == null) {
					fileResponseHeader.add(headerPrefix + "_" + i);
				} else {
					for (Map.Entry<String, InputAttribute> innerNestedAttrs : inputAttribute.attributesMap.entrySet()) {
						createResponseHeader(innerNestedAttrs, fileResponseHeader, headerPrefix + "_" + i);
					}
				}
			}
		} else {
			if (inputAttribute.attributesMap == null) {
				fileResponseHeader.add(headerPrefix);
			} else {
				for (Map.Entry<String, InputAttribute> innerNestedAttrs : inputAttribute.attributesMap.entrySet()) {
					createResponseHeader(innerNestedAttrs, fileResponseHeader, headerPrefix);
				}
			}
		}
	}

	private static void createAttributeMapFromProperties(BufferedReader reader, Map<String, InputAttribute> attributes)
			throws IOException {

		boolean eof = false;

		// Read Mapping attributes File
		while (!eof) {

			String line = reader.readLine();
			Integer noOfValues = 1;

			if (line == null) {
				break;
			} else if (line.contains("=")) {
				line = line.trim();
				String[] attrs = line.split("=", -1);
				if (attrs[1] != null && !attrs[1].isEmpty()) {
					noOfValues = Integer.parseInt(attrs[1]);
				}

				line = attrs[0].trim();
			}
			if (line.contains(".")) {
				String[] nestAttrs1 = line.split("\\.", -1);
				InputAttribute attribute = attributes.get(nestAttrs1[0]);
				if (attribute == null) {
					attribute = new InputAttribute();
				}
				createNestedExtractAttribute(nestAttrs1, 1, attribute, noOfValues);
				attributes.put(nestAttrs1[0], attribute);

			} else {
				InputAttribute inputAttribute = new InputAttribute();
				inputAttribute.count = noOfValues;

				attributes.put(line, inputAttribute);
			}
			// System.out.println("attributes in method : " + attributes);
		}
		reader.close();
	}

	/**
	 * 
	 * */
	private static void createNestedExtractAttribute(String[] attrs, Integer index, InputAttribute attribute,
			Integer noOfValues) {

		String attrName = attrs[index];

		if (attribute.attributesMap == null) {
			attribute.attributesMap = new LinkedHashMap<>();
		}
		InputAttribute nestAttr = attribute.attributesMap.get(attrName);

		if (nestAttr == null) {
			nestAttr = new InputAttribute();
		}

		if (attrs.length > (index + 1)) {
			createNestedExtractAttribute(attrs, index + 1, nestAttr, noOfValues);
		} else {
			nestAttr.count = noOfValues;
		}
		attribute.attributesMap.put(attrName, nestAttr);
	}

	public static boolean checkNull(String value) {
		if (value != null && !value.trim().equals("") && !value.trim().equals("UNKNOWN")
				&& !value.trim().equals("<blank>")) {
			return true;
		}
		return false;
	}

	// Validate and read the configuration properties file

	public static Properties loadProperties(String filepath) {

		FileReader fileReader = null;
		Properties properties = new Properties();

		try {

			File file = new File(filepath);

			if (!file.exists()) {
				logger.info(filepath
						+ " : Configuration file does not exist.Please provide the correct configuration file.");
				System.exit(0);
			}

			fileReader = new FileReader(file);
			properties.load(fileReader);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			try {
				fileReader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return properties;
	}

			public static void printPerformanceLog(long totalTasksExecuted, long totalTasksExecutionTime, long programStartTime,
			            long numberOfThreads) {
			        logger.info("[Performance]: ============= Current performance status (" + new Date().toString()
			                + ") =============");
			        long finalTime = System.currentTimeMillis() - programStartTime;
			        logger.info("[Performance]:  Total processing time : " + finalTime);

			        logger.info("[Performance]:  total task executed : " + totalTasksExecuted);
			        logger.info("[Performance]:  Total OPS (Entities sent for matching / Time spent from program start): "
			                + (totalTasksExecuted / (finalTime / 1000f)));
			        logger.info(
			                "[Performance]:  Total OPS without waiting for queue (Entities sent for matching/ (Time spent from program start - Time spent in waiting for API queue)): "
			                        + (totalTasksExecuted / ((finalTime) / 1000f)));
			        logger.info(
			                "[Performance]:  API Server data load requests OPS (Entities sent for matching/ (Sum of time spend by API requests / Threads count)): "
			                        + (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
			        logger.info(
			                "[Performance]: ===============================================================================================================");

			        // log performance only in separate logs
			        logPerformance.info("[Performance]: ============= Current performance status (" + new Date().toString()
			                + ") =============");
			        logPerformance.info("[Performance]:  Total processing time : " + finalTime);

			        logPerformance.info("[Performance]:  Entities sent: " + totalTasksExecuted);
			        logPerformance
			                .info("[Performance]:  Total OPS (Entities sent for matching / Time spent from program start): "
			                        + (totalTasksExecuted / (finalTime / 1000f)));
			        logPerformance
			                .info("[Performance]:  Total OPS without waiting for queue (Entities sent for matching / (Time spent from program start - Time spent in waiting for API queue)): "
			                        + (totalTasksExecuted / ((finalTime) / 1000f)));
			        logPerformance
			                .info("[Performance]:  API Server data load requests OPS (Entities sent for matching/ (Sum of time spend by API requests / Threads count)): "
			                        + (totalTasksExecuted / ((totalTasksExecutionTime / numberOfThreads) / 1000f)));
			        logPerformance.info(
			                "[Performance]: ===============================================================================================================");
			    }

}
