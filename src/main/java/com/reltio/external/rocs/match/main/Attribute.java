package com.reltio.external.rocs.match.main;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Attribute {
	public String type;
    public Map<String, List<Object>> value = new HashMap<String, List<Object>>();
	
    
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
    
	public Object getValue() {
		return value;
	}

	@SuppressWarnings("unchecked")
	public void setValue(Object value) {
		this.value = (Map<String, List<Object>>) value;
	}
	
}
