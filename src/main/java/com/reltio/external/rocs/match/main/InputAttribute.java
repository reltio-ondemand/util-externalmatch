package com.reltio.external.rocs.match.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InputAttribute {

	public List<String> attributes = new ArrayList<>(); 
	public Integer count = 1;
	public Map<String, InputAttribute> attributesMap = null;
	public Map<String, String> uriValuesMap = null;
	
}
