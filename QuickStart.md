# Quick Start 

##Building
The main method of the application is at the following   
path: com.reltio.external.rocs.match.main

##Dependencies 

1) log4j-slf4j-impl 1.7.25  
2)reltio-cst-core 1.4.6

##Parameters File Example
##Common properties file

```
ENVIRONMENT_URL=sndbx.reltio.com
TENANT_ID=OyqkxR3u1m5ytDT
USERNAME=smita.panda@reltio.com
PASSWORD=********
CLIENT_CREDENTIALS=*******
AUTH_URL=https://auth.reltio.com/oauth/token
ENTITIES_FILE_PATH=C:\\JavaPrograms\\util-externalmatch\\entities.json
OUTPUT_FILE=C:\\JavaPrograms\\util-externalmatch\\File1.csv
ATTRIBUTE_MAPPING_FILE = C:\\JavaPrograms\\util-externalmatch\\ATTmapping.txt
+HTTP_PROXY_HOST=
+HTTP_PROXY_PORT=

```
##Tool Specific Properties
##Attribute Mapping File Example

```
Name=3
dateFounded
CompanyType
Identifiers.Type
Identifiers.ID
Phone=2
Phone.Type
Phone.Number
Phone.CountryCode

```

##Executing

Command to start the utility.
```
#!plaintext

#Windows
java   -jar   util-externalmatch-1.0.5-jar-with-dependencies.jar  â€œconfiguration.propertiesâ€ > $logfilepath$'

#Unix
java   -jar   util-externalmatch-1.0.5-jar-with-dependencies.jar r â€œconfiguration.propertiesâ€ > $logfilepath$'

```